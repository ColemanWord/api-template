package design                                     // The convention consists of naming the design
                                                   // package "design"
import (
        . "github.com/goadesign/goa/design"        // Use . imports to enable the DSL
        . "github.com/goadesign/goa/design/apidsl"
)

var _ = API("cellar", func() {                     // API defines the microservice endpoint and
        Title("The virtual wine cellar")           // other global properties. There should be one
        Description("A simple goa service")        // and exactly one API definition appearing in
        Scheme("http")                             // the design.
        Host("localhost:8080")
})
var BottlePayload = Type("Bottle Payload", func() {
	Description("BottlePayload is the type used to create bottles")

	Attribute("name", String, "Name of bottle", func(){
		MinLength(2)
	})
	Attribute("vintage", Integer, "Vintage of bottle" func() {
		Minimum(1900)
	})
	Attribute("rating", Integer, "Rating of bottle", func() {
		Minimum(1)
		Maximum(5)
	})
	Required("name", "vintage", "rating")
})

// BottleMedia defines the media type used to render bottles.
var BottleMedia = MediaType("application/vnd.gophercon.goa.bottle", func() {
        Name("bottle")
        Reference(BottlePayload)
        
        Attributes(func() {                         // Attributes define the media type shape.
                Attribute("ID", Integer, "Unique bottle ID")
                Attribute("name")
                Attribute("vintage")
                Attribute("rating")
                Required("ID", "name", "vintage", "rating")
        })
})       
var _ = Resource("bottle", func() {
        Description("a bottle of wine")
        BasePath("/bottles")
        
        Action("create", func() {
                Description("creates a bottle")
                Routing(POST("/"))
                Payload(BottlePayload)
                Response(Created)
        })
        Action("show", func() {
                Description("shows a bottle")
                Routing(Get("/:id"))
                Params(func() {
                        Param("id", Integer)
                })
                Response(OK, BottleMedia)
        })
})